require 'picopc'
require 'pry'
require 'bunny'

tag_gas_before_boiler = 'COM6.RMT69_A1[1].gas_before_boiler'
tag_drum_water_level = 'COM6.RMT69_A2[2].drum_water_level'
tag_superheat_steam_pressure_after_boiler = 'COM6.RMT69_A3[3].supheat_steam_pressure_after_boiler'
tag_temp_steam_after_boiler = 'COM6.RMT69_A4[4].temp_steam_after_boiler'
tag_temp_steam_after_boiler2 = 'COM6.RMT69_A5[5].temp_steam_after_boiler2'
tag_flow_superheat_steam_after_boiler = 'COM6.RMT69_A6[6].flow_supheat_steam_after_boiler'
tag_flow_feedwater_on_boiler = 'COM6.RMT69_A7[7].flow_feedwater_on_boiler'
tag_temp_feedwater_and_exh_gases = 'COM6.RMT69_A8[8].temp_feedwater_and_exh_gases'
tag_press_feedwater_on_boiler = 'COM6.RMT69_A9[9].press_feedwater_on_boiler'
tag_flowrate_air_before_burners = 'COM6.RMT69_A10[10].flowrate_air_before_burners'

opc = PicOpc::Client.new 'OPCDataStore.TOPCElemerServer.2'

con = Bunny.new "amqp://boiler:hboil37@192.168.254.125"
con.start
ch = con.create_channel
x = ch.fanout('vals')
q = ch.queue("",exclusive:true, arguments:{"x-message-ttl" => 10000})

i = 0
begin
loop do
	if i % 10 == 0
		 puts "Query values..."
		 i = 0
	end
	i += 1

	gas_before_boiler = opc.read tag_gas_before_boiler 
	drum_water_level = opc.read tag_drum_water_level 
	superheat_steam_pressure_after_boiler = opc.read tag_superheat_steam_pressure_after_boiler 
	temp_steam_after_boiler = opc.read tag_temp_steam_after_boiler 
	temp_steam_after_boiler2 = opc.read tag_temp_steam_after_boiler2 
	flow_superheat_steam_after_boiler = opc.read tag_flow_superheat_steam_after_boiler 
	flow_feedwater_on_boiler = opc.read tag_flow_feedwater_on_boiler 
	temp_feedwater_and_exh_gases = opc.read tag_temp_feedwater_and_exh_gases 
	press_feedwater_on_boiler = opc.read tag_press_feedwater_on_boiler 
	flowrate_air_before_burners = opc.read tag_flowrate_air_before_burners 

=begin
puts "gas_before_boiler #{gas_before_boiler.to_f }"
puts "drum_water_level #{drum_water_level.to_f.round(3) }"
puts "superheat_steam_pressure_after_boiler #{superheat_steam_pressure_after_boiler }"
puts "temp_steam_after_boiler #{temp_steam_after_boiler }"
puts "temp_steam_after_boiler2 #{temp_steam_after_boiler2 }"
puts "flow_superheat_steam_after_boiler #{flow_superheat_steam_after_boiler }"
puts "flow_feedwater_on_boiler #{flow_feedwater_on_boiler }"
puts "temp_feedwater_and_exh_gases #{temp_feedwater_and_exh_gases }"
puts "press_feedwater_on_boiler #{press_feedwater_on_boiler }"
puts "flowrate_air_before_burners #{flowrate_air_before_burners }"
=end


x.publish "1 gas_before_boiler #{gas_before_boiler.gsub(',','.').to_f().round(2) }"
x.publish "2 drum_water_level #{drum_water_level.gsub(',','.').to_f().round(2) }"
x.publish "3 superheat_steam_pressure_after_boiler #{superheat_steam_pressure_after_boiler.gsub(',','.').to_f().round(2) }"
x.publish "4 temp_steam_after_boiler #{temp_steam_after_boiler.gsub(',','.').to_f().round(2) }"
x.publish "5 temp_steam_after_boiler2 #{temp_steam_after_boiler2.gsub(',','.').to_f().round(2) }"
x.publish "6 flow_superheat_steam_after_boiler #{flow_superheat_steam_after_boiler.gsub(',','.').to_f().round(2) }"
x.publish "7 flow_feedwater_on_boiler #{flow_feedwater_on_boiler.gsub(',','.').to_f().round(2) }"
x.publish "8 temp_feedwater_and_exh_gases #{temp_feedwater_and_exh_gases.gsub(',','.').to_f().round(2) }"
x.publish "9 press_feedwater_on_boiler #{press_feedwater_on_boiler.gsub(',','.').to_f().round(2) }"
x.publish "10 flowrate_air_before_burners #{flowrate_air_before_burners.gsub(',','.').to_f().round(2) }"

  1.times do 
  	print '.'
	sleep 1
  end
end
ensure
	opc.cleanup

con.close
end
